export default function () {
    return {
        state: {
            count: 0
        },
        mutations: {
            increment: state => state.count++,
            decrement: state => state.count--
        },
        getters: {
            blah (state) {
                return state.count;
            }
        },
        actions: {
            increment ({commit, state}) {
                commit('increment');
            }
        }
    }
}
